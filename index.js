'use strict';

const path = require('path');
const fs = require('fs');
const crypto = require('crypto');
const util = require('util');
const express = require('express');
const cookieparser = require('cookie-parser');
const httpproxy = require('http-proxy');
const outgoing = require('http-proxy/lib/http-proxy/passes/web-outgoing');
const harmon = require('harmon');
const minimist = require('minimist');
const basex = require('base-x');

const config = minimist(process.argv.slice(2), {
	default: {
		port: 8080,
		host: '127.0.0.1',
		fileroot: 'config',
		headsel: 'head',
		headmod: '.headmod.txt',
		authform: '.basicauth.html',
		authcookie: 'pwaxyBasicAuth',
		ws: true,
		xfwd: true,
		changeOrigin: true
	}
});
Object.keys(config)
	.filter(k => k.startsWith('no-') && config[k])
	.forEach(k => delete config[k.substr('no-'.length)]);

const reqd = { port: 1, host: 1, fileroot: 1, target: 1 };
const missing = Object.keys(reqd)
	.filter(k => !config.hasOwnProperty(k));
if(missing.length)
	throw Error(`missing required config options: ${missing.map(x=>'--'+x).join(', ')}`);

const proxyopts = {
	target: config.target,
	ws: config.ws,
	xfwd: config.xfwd,
	changeOrigin: config.changeOrigin,
	selfHandleResponse: !!config.authform,
};

const readFile = util.promisify(fs.readFile);
const base58 = basex('123456789ABCDEFGHJKLMNPQRSTUVWXYZabcdefghijkmnopqrstuvwxyz')
	.encode;

const app = express();

const proxy = httpproxy.createProxyServer(proxyopts);

const basic = 'Basic ';

function parseauth(auth) {
	auth = `${auth}`;
	if(!auth.startsWith(basic)) return auth;
	try {
		const [user, pass] = Buffer.from(auth.substr(basic.length), 'base64')
			.toString()
			.split(':', 2);
		return { user, pass };
	} catch (e) {}
}

function handleauth(auth) {
	if(!config.passhash) return auth;
	const creds = parseauth(auth);
	if(!creds) return auth;
	const hash = base58(crypto.createHmac('sha256', creds.pass)
			.update(config.passhash)
			.digest())
		.substr(0, 22); // just over 128 bits
	if(config.passhashlog)
		console.log({ passhash: { user: creds.user, hash } });
	return basic + Buffer.from(`${creds.user}:${hash}`)
		.toString('base64');
}

if(config.authcookie) {
	app.use(cookieparser());
	app.use((req, res, next) => {
		if(req.cookies && req.cookies[config.authcookie])
			req.headers.authorization = handleauth(req.cookies[config.authcookie]);
		next();
	});
}

if(config.authform)
	proxy.on('proxyRes', (pr, req, res) => {
		if(pr.statusCode !== 401) {
			Object.values(outgoing)
				.forEach(x => x(req, res, pr, proxyopts));
			return pr.pipe(res);
		}
		res.statusCode = 200;
		res.sendFile(path.join(__dirname, config.fileroot,
			config.authform), { dotfiles: 'allow' });
	});

app.use(express.static(path.join(__dirname, config.fileroot)));

if(config.authstatic) {
	const staticauth = async (auth) => {
		const creds = parseauth(auth);
		if(!creds) throw 0;
		const content = await fs.promises.readFile(path.join(__dirname, config.fileroot,
			config.authstatic));
		if(content.toString()
			.split('\n')
			.map(x => x.trim())
			.filter(x => x)
			.find(x => x === `${creds.user}:${creds.pass}`))
			return;
		throw 0;
	};
	const skip = config.authskip && `${config.authskip}`.split(':').map(x => x.trim()).filter(x => x);
	app.use(async (req, res, next) => {
		if(skip && skip.find(x => req.path.startsWith(x)))
			return next();
		try {
			await staticauth(req.headers.authorization);
		} catch (err) {
			if(config.authform) {
				res.statusCode = 200;
				res.sendFile(path.join(__dirname, config.fileroot,
					config.authform), { dotfiles: 'allow' });
			} else {
				res.set('WWW-Authenticate', 'Basic realm="PWAxy"');
				res.sendStatus(401);
				res.end();
			}
			return;
		}
		next();
	});
}

if(config.headsel && config.headmod)
	app.use(harmon([], [{
		query: config.headsel,
		func: async node => {
			const str = node.createStream();
			const parts = await Promise.all([
				(async () => {
					let buff = '';
					str.on('data', x => buff += x.toString());
					await new Promise(r => str.on('end', r));
					return buff;
				})(),
				readFile(path.join(config.fileroot, config.headmod))
				.then(x => x.toString()),
			]);
			str.end(parts.join(''));
		}
	}]));

app.use((req, res) => proxy.web(req, res));

app.listen(config.port, config.host);
