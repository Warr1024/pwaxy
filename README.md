# PWAxy: PWA Reverse Proxy

PWAxy is intended to add some limited Progressive Web App
capabilities to existing simple web apps, mainly to allow them
to be installed as desktop/mobile apps in standalone mode.  This
is accomplished by serving PWA resources, including the manifest,
service worker, icon, and offline page, and routing all other
requests to the back-end app.

All of the PWA resources are meant to be customized; the files
shipped with this repo are placeholders/examples.

## HTTP Basic Auth Workaround

PWAxy also provides some support for back-end apps that rely on
HTTP Basic auth, which PWA's do not support well natively.

The embedded browser that serves standalone PWA's does not provide
a login prompt for HTTP Basic auth, and instead just renders the
401 error response.  Users also may not have control over the
cache lifetime of credentials supplied at app install time, so
an app relying on Basic auth may just stop working at any time.

PWAxy intercepts 401 responses, displays its own HTML-based form,
and then stores the Basic credentials as a cookie, and then
translates that cookie into a normal Authorization header to the
backend site, allowing login within the PWA, and even installation
of the PWA before login.

### Optional Password Pre-Hashing

Some backend apps don't store user credentials very securely, e.g.
TiddlyWiki server, which only supports plaintext methods (command
line, CSV file).  Using the `--passhash=...` option, PWAxy can
pre-hash passwords so you can store hashes in these storage
mechanisms instead.  PWAxy will hash the password and then pass the
hashed password to the backend.

Note that this only effectively prevents the reverse engineering
of the original passwords in the event the password database is
stolen; it does not protect against a "pass the hash" attack
allowing someone to log in to the live service.  This may still be
valuable to mitigate password reuse across services.

To set it up:

- Choose a strong random passhash value and give it to PWAxy on
  the command line at startup via `--passhash=...`, along with
  `--passhashlog`.
- Open the app through PWAxy so you get a login prompt.
- While monitoring the PWAxy console output, enter and submit the
  credentials you want to use on the site.  PWAxy will dump the
  surrogate user/pass combo in the console logs.
- Set the username/password of the backend application to the
  values dumped by PWAxy.
- Restart PWAxy without the `--passhashlog` command line option
  to run in production mode.

## Usage

```bash
node . <options>
```

It won't work unless at least the `--target=...` option is supplied,
as it's required and there is no default value.

All options can be prefixed with `no-` (e.g. `--no-ws` to disable
the `--ws` option that's on by default).  `--no-X` options always
override `--X` options.  Note that removing some options entirely
like `--port=` or `--fileroot=` will cause PWAxy not to work.

Options:

- `--target=http://...` - set the backend app target URL for the proxy.
- `--port=...` - set the port for PWAxy to listen on, default `8080`.
- `--host=...` - set the host/IP to bind PWAxy to, default `127.0.0.1`.
- `--fileroot=...` - filesystem path to the root of the static file store,
  default `config` (relative to cwd).
	- dotfiles inside the file root are not served directly to the
	  web client, and are used for special-purpose content like the
	  basic auth form and header modification content.
	- non-dotfiles inside the file root are served to the client
	  directly, *bypassing* the proxy, without requiring any
	  authentication.  This is how extra files needed for the PWA
	  like app manifests and service worker scripts are served.
	- *If your backend web app has a path prefix:*
	  non-dotfiles are searched by the full path in the URL, so if
	  you are serving an app at `/app/` that needs a service worker
	  at absolute URL path `/app/service-worker.js`, you will need
	  to create an `app` subdir inside the fileroot and store the
	  relevant files in there.
- `--headsel=...` - CSS selector to find the head element to append the
  HTML header modifications to, default `head`.  Disabling either this
  or `--headmod` disables HTML header injection.
- `--headmod=...` - filesystem path relative to `fileroot` where the
  content to insert into HTML headers is stored, default `.headmod.txt`.
  Disabling either this or `--headsel` disables HTML header injection.
- `--authform=...` - filesystem path relative to `fileroot` where the
  HTTP Basic auth form HTML is served from, default `.basicauth.html`.
  Disabling this disables 401 response rewriting, so the original 401
  requesting Basic auth is delivered to the client.
- `--authcookie=...` - name of the cookie containing HTTP auth credentials
  from the `--authform` frontend, default `pwaxyBasicAuth`.
- `--authstatic=...` - filesystem path relative to `fileroot` where a
  static list of authorized users (in user:pass format, one per line)
  can be found.  If specified, only users matching any entry in this
  file will be allowed *in addition* to any access control imposed by
  the underlying application.  Defaults to disabled.
- `--authskip=...:...:...` - colon-delimited set of exact-matching
  absolute path prefixes to skip for `--authstatic` access control,
  e.g. to allow access to service workers, manifests, assets, etc.
  that may be defined by the underlying application.  Does not affect
  local static files (already exempted) nor auth done by the
  underlying application itself (cannot be exempted).
- `--ws` - enable websocket passthru, default true.
- `--xfwd` - enable X-Forwarded-* headers, default true.
- `--changeOrigin` - rewrite request headers to the correct
  values as if the backend were queried directly, i.e. set Host to the
  backend hostname, not the proxy hostname as sent by the client,
  default true.
- `--passhash=...` - set a key for basic auth password hashing when using
  the authform/authcookie Basic auth workaround, default to disabled.
  See the **Password Pre-Hashing** section above.
- `--passhashlog` - dump surrogate credentials from password hashing
  to the console, so you can use PWAxy itself to setup your backend
  app password database.
  See the **Password Pre-Hashing** section above.

## Customizing

The files supplied in `config` are meant as examples/templates only,
and you are meant to use custom replacements.

At minimum, you will certainly want to modify the manifest (supply
your own names/descriptions) and replace the icon (give it a unique
appearance so it can be found quickly from an app list).
