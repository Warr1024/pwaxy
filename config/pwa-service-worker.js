'use strict';

const aliveEvent = (name, cb) => self.addEventListener(name, event => {
	const p = cb(event);
	if(p && event.waitUntil) return event.waitUntil(p);
	return p;
});

const log = msg => {
	self.clients.matchAll()
		.then(cl => cl.forEach(c => c.postMessage(`SW: ${msg}`)));
};

aliveEvent('install', event => {
	log('install');
	event.waitUntil((async () => {
		const cache = await caches.open('offline');
		await cache.add(new Request('pwa-offline.html', { cache: 'reload' }));
	})());
	return self.skipWaiting();
});

aliveEvent('activate', () => {
	log('activate');
	return self.clients.claim();
});

self.addEventListener('fetch', (event) => {
	if(event.request.mode !== 'navigate') return;
	event.respondWith((async () => {
		try {
			const preloadResponse = await event.preloadResponse;
			if(preloadResponse) return preloadResponse;
			const networkResponse = await fetch(event.request);
			return networkResponse;
		} catch (err) {
			const cache = await caches.open('offline');
			const cachedResponse = await cache.match('pwa-offline.html');
			return cachedResponse;
		}
	})());
});
