<link rel="manifest" href="pwa-manifest.json" />
<script type="text/javascript">
	if(navigator.serviceWorker) {
	  navigator.serviceWorker.addEventListener('message', e => console.log(e.data));
	  const log = (...x) => console.log(x);
	  navigator.serviceWorker.register('pwa-service-worker.js').then(log);
	  navigator.serviceWorker.ready.then(() => log('serviceWorker ready')).catch(log);
	}
	window.addEventListener('beforeinstallprompt', event => event.prompt());
	window.addEventListener('appinstalled', event => console.log('appinstalled', event));
</script>